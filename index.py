#!/usr/bin/env Python3
# -*- coding: gb2312 -*-

"""
# Danny 2018-11-30 Aiitec
# 从国家统计局网站抓取{省市区街道居委会}五级地理位置数据
# http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/index.html
"""

import os

from fetchHTML import startZhuQu
from fetchHTML import startZhuQuFailURL
from fetchHTML import startFetchVillageWithThread
from func import logListHandle

# 1~4 为基本设置项

# 1. 这里面的my_headers中的内容，由于是个人主机的信息，所以我就用句号省略了一些，在使用时可以将自己主机的User-Agent放进去
my_headers = [
    "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Mobile Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"]

# 2. 国家统计局 2017年统计用区划代码和城乡划分代码(截止2017年10月31日)
base_url = "http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/"

# 3. 编码。国家统计局网站采用gb2312编码
encoded="gb2312"

# 4. 数据存储的位置
# os.makedirs("data/", 0o777)
# - {省市区街道}四级数据存储位置
dataFile = "data/1234.sql"
#os.remove(dataFile)
# - {居委会}五级数据存储位置
dataFile5 = "data/5.sql"
#os.remove(dataFile5)
# - 统计存储位置
statFile = "data/stat.log"
#os.remove(statFile)


# 一、抓取{省市区街道}四级数据
#count = startZhuQu(base_url, dataFile, my_headers)
#count = startZhuQu(base_url, dataFile, my_headers, 1)
#count = startZhuQu(base_url, dataFile, my_headers, 2)
#count = startZhuQu(base_url, dataFile, my_headers, 3)
#count = startZhuQu(base_url, dataFile, my_headers, 4)
#count = startZhuQu(base_url, dataFile, my_headers, 5)
#count = startZhuQu(base_url, dataFile, my_headers, 6)
#print(count)

# 二、重新抓取失败的URL{省市区街道}四级数据（仅重新抓取一次有效，不然多次重新抓取失败的URL，数据会有重复）
#logFile = open("data/log-error.log", "r", encoding=encoded)
#log = logFile.read()
#data = logListHandle(log)
#print(data)
#count = startZhuQuFailURL(base_url, dataFile, my_headers, 4, data)
#print(count)

# 三、1. 仅抓取五级{居委会}全部数据
#count = startZhuQu(base_url, dataFile5, my_headers, 6)
#print(count)
# 2. 仅抓取五级{居委会}指定数据
#count = startZhuQu(base_url, dataFile5, my_headers, 6, 1, ['11'])
#count = startZhuQu(base_url, dataFile5, my_headers, 6, 2, ['4419', '4420', '4604']) # 广东省东莞市 广东省中山市 海南省儋州市
#count = startZhuQu(base_url, dataFile5, my_headers, 6, 3, ['110101', '110102'])
#count = startZhuQu(base_url, dataFile5, my_headers, 6, 4, ['441900124'])
#print(count)

# 四、重新抓取失败的URL{居委会}五级数据（仅重新抓取一次有效，不然多次重新抓取失败的URL，数据会有重复）
#logFile = open("data/log-error.log", "r", encoding=encoded)
#log = logFile.read()
#data = logListHandle(log)
# data = {'province': [], 'city': ['3307', '4604', '5111'], 'county': ['431302'], 'town': ['130207201', '131124203']}
#print(data)
#count = startZhuQuFailURL(base_url, dataFile5, my_headers, 6, data)
#print(count)

# 五、1. 统计{省市区街道} 数据
#count = startZhuQu(base_url, statFile, my_headers, 0, 0, [], True, encoding=encoded)
# 2. 统计{居委会} 数据
# count = startZhuQu(base_url, statFile, my_headers, 6, 0, [], True, encoding=encoded)
#count = startZhuQu(base_url, statFile, my_headers, 6, 1, ['13'], True, encoding=encoded)
#print(count)

# 六、1.重新统计失败的URL{省市区街道}四级数据（仅重新统计一次有效，不然多次重新统计失败的URL，数据会有重复）
"""
logFile = open("data/log-error.log", "r", encoding=encoded)
log = logFile.read()
data = logListHandle(log)
#data = {'province': ['41','42','43','45','50','62'], 'city': [], 'county': [], 'town': []}
print(data)
#count = startZhuQuFailURL(base_url, statFile, my_headers, 4, data, True)
# 2. 重新统计{居委会} 数据
count = startZhuQuFailURL(base_url, statFile, my_headers, 6, data, True)
print(count)
"""

# 七、多线程抓取数据
#data = {'province': ['11','12','13','14','15','21','22','23','31','32','33','34','35','36','37','41','42','43','44','45','46','50','51','52','53','54','61','62','63','64','65'], 'city': [], 'county': [], 'town': []}
#data = {'province': ['13','14','15'], 'city': [], 'county': [], 'town': []}
#  data = {'province': ['61','62','63','64','65'], 'city': [], 'county': [], 'town': []}
#startFetchVillageWithThread(base_url, "data/", my_headers, 6, 1, data, True)

