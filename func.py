#!/usr/bin/env Python3
# -*- coding: gb2312 -*-

"""
# Danny 2018-11-30 Aiitec
# 从国家统计局网站抓取{省市区街道居委会}五级地理位置数据
# http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/index.html
"""

import urllib, urllib.request, urllib.error, socket, random, time, re, json

"""
方法一 抓取网页内容
# url：抓取的初始网页地址{省}
# headers：主机的User-Agent数组，用于防止目标服务器防爬虫

# return content 返回抓取的网页内容
"""
def fetchHTML(url, headers):
    """
    此函数用于抓取返回403禁止访问的网页
    """
    random_header = random.choice(headers)

    content = "" # 抓取的网页内容
    error_massage = ""

    # 获得当前时间时间戳
    now = int(time.time())
    # 转换为其他日期格式,如:"%Y-%m-%d %H:%M:%S"
    timeStruct = time.localtime(now)
    humanTime = time.strftime("%Y-%m-%d %H:%M:%S", timeStruct)
    # print(humanTime)

    try:
        """
        对于Request中的第二个参数headers，它是字典型参数，所以在传入时
        也可以直接将个字典传入，字典中就是下面元组的键值对应
        """
        req = urllib.request.Request(url)
        req.add_header("User-Agent", random_header)
        req.add_header("GET", url)
        req.add_header("Host", "www.stats.gov.cn")
        req.add_header("Referer", "http://www.stats.gov.cn/")

        response = urllib.request.urlopen(req)

    except urllib.error.HTTPError as e:
        error_massage = "["+ humanTime +" HTTPError] " + url + "\r\n"
        print(url, "\r\n 404 Not Found, Error code:", e.code)

    except urllib.error.URLError as e:
        error_massage = "["+ humanTime +" URLError] " + url + "\r\n"
        print(url, "\r\n we failed to reach a server, reason:", e.reason)

    except socket.timeout as e:
        error_massage = "["+ humanTime +" socket.timeout] " + url + "\r\n"
        print(url, "\r\n socket.timeout: ", e)

    else:
        content = response.read()
        content = content.decode("gb2312", "ignore")
        # content = content.decode("gb2312").encode("utf8")

    if len(error_massage) > 0:
        #f_url = open("data/log-error-"+ str(time.time()) +".log", "a+", encoding="gb2312")
        f_url = open("data/log-error.log", "a+", encoding="gb2312")
        #f_url.write("[HTTPError] " + url + "\r\n")
        f_url.write(error_massage)
        f_url.close()

    return content

"""
方法二 解析错误日志记录，并返回数据列表
# log: 错误日志内容

# return data 返回格式化后的数据列表
"""
def logListHandle(log):

    # 定义返回的数据字典格式
    # data["province"] = [11,12]
    # data["city"] = [1303,1404]
    # data["county"] = [440103,440104,450302]
    # data["town"] = [440103009, 440103011, 450302001]

    data = {"province": [], "city": [], "county": [], "town": []}

    # 1. 替换多余的字符串，如"[HTTPError] ","[URLError] ","[socket.timeout] ","http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/"
    repList = ["http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/",".html"]
    # 正则匹配，替换
    log = re.sub(r"\[.*\] ", "", log)
    log = log.replace(repList[0], "")
    log = log.replace(repList[1], "")
    log = log.rstrip()
    # 2. 获取一、二、三、四维数组
    urlList = log.split("\n")
    #print(urlList)
    # 省完全没有抓取的编码code
    list1 = []
    # 市完全没有抓取的编码code
    list2 = []
    # 区完全没有抓取的编码code
    list3 = []
    # 街道完全没有抓取的编码code
    list4 = []

    # 3. 处理数组，目标格式为：
    for url in urlList:
        tmp = url.split("/")
        if len(tmp) == 1:
            list1.append(tmp[0])
        elif len(tmp) == 2:
            list2.append(tmp[1])
        elif len(tmp) == 3:
            list3.append(tmp[2])
        elif len(tmp) == 4:
            list4.append(tmp[3])

    data["province"] = list1
    data["city"] = list2
    data["county"] = list3
    data["town"] = list4

    # 获得当前时间时间戳
    now = int(time.time())
    # 转换为其他日期格式,如:"%Y-%m-%d %H:%M:%S"
    timeStruct = time.localtime(now)
    humanTime = time.strftime("%Y-%m-%d %H:%M:%S", timeStruct)
    # print(humanTime)

    f_url = open("data/stat-error-data.log", "a+", encoding="gb2312")
    f_url.write(humanTime + "\r\n")
    f_url.write(json.dumps(data) + "\r\n")
    f_url.close()

    # 4. 返回列表
    return data

"""
方法三 数据列表去重

# return newList 返回去重后的数据列表
"""
def listHandle(list, start, end):
    newList = []
    for d in list:
        tmp = str(d)[start:end]
        if len(tmp) > 0 and tmp not in newList:
            newList.append(int(str(d)[start:end]))
    return newList;

