#!/usr/bin/env Python3
# -*- coding: gb2312 -*-

"""
# Danny 2018-11-30 Aiitec
# 从国家统计局网站抓取{省市区街道居委会}五级地理位置数据
# http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/index.html
"""

import sys, os, re,  random, urllib.parse, urllib.request, urllib.error, socket, time, math, threading, queue
from urllib.request import urlopen

from enum import Enum
import func
from func import fetchHTML
from func import logListHandle
from func import listHandle

# 基本设置

timeout = 30
socket.setdefaulttimeout(timeout)

# 枚举
Level = Enum('Level','province city county town village')


"""
方法一 抓取{省市区街道居委会}五级地理位置数据
# url：抓取的初始网页地址{省}
# file：抓取的数据存储位置
# headers：主机的User-Agent数组，用于防止目标服务器防爬虫
# deep：抓取深度
        0：默认，抓取{省市区街道}四级数据  
        1：抓取{省}一级数据  
        2：抓取{省市}二级数据  
        3：抓取{省市区}三级数据 
        4：抓取{省市区街道}四级数据  
        5：抓取{省市区街道居委会}五级数据 （Warn: 由于{省市区街道}和{居委会}的数据表结构不一样，故暂不支持）
        6：仅抓取{居委会}第五级数据
# level：从第几级以下开始抓取，不包含自己本身数据（重新抓取失败的URL时需要）。
        0：默认，抓取全部数据
        1：抓取{省}下级全部数据
        2：抓取{市}下级全部数据
        3：抓取{区}下级全部数据
        4：抓取{街道}下级全部数据
# data：需要重新抓取的数据列表（重新抓取失败的URL时需要）。
# onlyStat：仅统计数据量，不实际写入数据文件（对抓举的数据完整性进行校验时需要）。
# encoding：抓取的网页编码，默认"gb2312"

# return 返回数据总条数
"""
def startZhuQu(url, file, headers, deep=0, level=0, data=[], onlyStat=False, encoding="gb2312"):

    if deep == 5:
        print("Warn: 由于{省市区街道}和{居委会}的数据表结构不一样，故暂不支持")
        return

    timeStart = time.perf_counter()
    print("startZhuQu timeStart: " + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))

    # 返回数据总条数
    stat = 0
    # 记录 没有区(广东省中山市，广东省东莞市，海南省儋州市 直接是镇)的数据条数
    stat_no_county = 0
    stat_province = 0
    stat_city = 0
    stat_county = 0
    stat_town = 0
    stat_village = 0

    """
    省市区街道居委会 HTML解析规则
    """
    re_province_1 = "<td><a href='(.+?).html'>"
    re_province_2 = "<td><a href='.+?.html'>(.+?)<br/></a></td>"
    re_city_1 = "<tr class='citytr'><td><a href='.+?/(.+?).html'>.+?</a></td><td><a href='.+?.html'>.+?</a></td></tr>"
    re_city_2 = "<tr class='citytr'><td><a href='.+?.html'>.+?</a></td><td><a href='.+?.html'>(.+?)</a></td></tr>"
    re_county_1 = "<tr class='countytr'><td><a href='.+?.html'>.+?</a></td><td><a href='.+?/(.+?).html'>.+?</a></td></tr>"
    re_county_2 = "<tr class='countytr'><td><a href='.+?.html'>.+?</a></td><td><a href='.+?.html'>(.+?)</a></td></tr>"
    re_town_1 = "<tr class='towntr'><td><a href='.+?.html'>(.+?)</a></td><td><a href='.+?.html'>.+?</a></td></tr>"
    re_town_2 = "<tr class='towntr'><td><a href='.+?.html'>.+?</a></td><td><a href='.+?.html'>(.+?)</a></td></tr>"
    re_village_1 = "<tr class='villagetr'><td>(.+?)</td><td>.+?</td><td>.+?</td></tr>"
    re_village_2 = "<tr class='villagetr'><td>.+?</td><td>.+?</td><td>(.+?)</td></tr>"
    re_village_3 = "<tr class='villagetr'><td>.+?</td><td>(.+?)</td><td>.+?</td></tr>"

    f = open(file, "a+", encoding=encoding)

    html = fetchHTML(url, headers)
    codes = re.findall(re_province_1, html)
    names = re.findall(re_province_2, html)

    base_sql = ""
    if deep < 5:
        base_sql = "INSERT INTO `china_region` (`code` ,`name`) VALUES \r\n"
    elif deep == 6:
        base_sql = "INSERT INTO `china_region_village` (`code` ,`name` ,`code2`) VALUES \r\n"

    if onlyStat:
        f.write("Start Stat. \r\n")
    else:
        f.write(base_sql)

    # 设置多少条数据开始一个新的SQL语句
    statement = 5000

    # 省
    for code, name in zip(codes, names):
        # if int(code) != 37:
        # if int(code) not in [44, 46]:
        #    continue

        # 统计数据初始化
        stat_province = 0
        stat_city = 0
        stat_county = 0
        stat_town = 0
        stat_village = 0

        if level in [1, 2, 3, 4]:
            if int(code) not in listHandle(data, 0, 2):
                continue

        if (level == 0 or pow(2, level) < pow(2, 1)) and (deep >= 0 and deep < 6):
            stat += 1
            stat_province += 1
            if onlyStat != True:
                txt = "('" + str(code) + "', '" + name + "'), \r\n"
                f.write(txt)

        if deep != 0 and deep <= 1:
            continue

        url2 = url + str(code) + ".html"
        print(url2)
        html2 = func.fetchHTML(url2, headers)
        codes2 = re.findall(re_city_1, html2)
        names2 = re.findall(re_city_2, html2)

        # 市
        for code2, name2 in zip(codes2, names2):
            # 删
            # if int(code2) not in [4419, 4420, 4604]:
            #    continue

            if level in [2, 3, 4]:
                if int(code2) not in listHandle(data, 0, 4):
                    continue

            if (level == 0 or pow(2, level) < pow(2, 2)) and (deep == 0 or (deep >= 2 and deep < 6)):
                stat += 1
                stat_city += 1
                if onlyStat != True:
                    txt2 = "('" + str(code2) + "', '" + name2 + "'), \r\n"
                    f.write(txt2)

            if deep != 0 and deep <= 2:
                continue

            url3 = url + str(code) + "/" + str(code2) + ".html"
            print(url3)
            html3 = fetchHTML(url3, headers)
            codes3 = re.findall(re_county_1, html3)
            names3 = re.findall(re_county_2, html3)

            # 1.考虑没有区的情况(广东省中山市，广东省东莞市，海南省儋州市)，直接是镇
            if len(codes3) == 0:
                stat_no_county += 1
                print("没有区的情况：code2 = " + code2 + ", continue;")

                if deep != 0 and deep <= 3:
                    continue

                html4 = fetchHTML(url3, headers)
                codes4 = re.findall(re_town_1, html4)
                names4 = re.findall(re_town_2, html4)

                if (deep >= 4 and deep < 6) and onlyStat:
                    stat += len(codes4)
                    stat_town += len(codes4)
                    continue

                txt4 = ""
                for code4, name4 in zip(codes4, names4):
                    if level in [4]:
                        if int(str(code4)[0:9]) not in listHandle(data, 0, 9):
                            continue

                    if (level == 0 or pow(2, level) < pow(2, 4)) and  (deep == 0 or (deep >= 4 and deep < 6)):
                        stat += 1
                        stat_town += 1
                        if onlyStat != True:
                            if stat % statement == 0:
                               txt4 += "('" + str(code4) + "', '" + name4 + "'); \r\n" + base_sql
                            else:
                                txt4 += "('" + str(code4) + "', '" + name4 + "'), \r\n"
                            #f.write(txt4)

                    if deep >= 5:
                        url5 = url + str(code) + "/" + str(code2)[2:4]  + "/" + str(code4)[0:9] + ".html"
                        print(url5)
                        html5 = fetchHTML(url5, headers)
                        codes5 = re.findall(re_village_1, html5)
                        names5 = re.findall(re_village_2, html5)
                        codes52 = re.findall(re_village_3, html5)

                        stat += len(codes5)
                        stat_village += len(codes5)
                        if onlyStat != True:
                            txt5 = ""
                            for code5, name5, code52 in zip(codes5, names5, codes52):
                                if stat % statement == 0:
                                    txt5 += "('" + str(code5) + "', '" + name5 + "', '" + str(
                                        code52) + "'); \r\n" + base_sql
                                else:
                                    txt5 += "('" + str(code5) + "', '" + name5 + "', '" + str(code52) + "'), \r\n"
                            f.write(txt5)
                f.write(txt4)

                continue
            # 2.默认，有区的情况
            for code3, name3 in zip(codes3, names3):
                # if int(code3) not in [610429, 620103]:
                #    continue

                if level in [3, 4]:
                    if int(code3) not in listHandle(data, 0, 6):
                        continue

                if (level == 0 or pow(2, level) < pow(2, 3)) and  (deep == 0 or (deep >= 3 and deep < 6)):
                    stat += 1
                    stat_county += 1
                    if onlyStat != True:
                        txt3 = "('" + str(code3) + "', '" + name3 + "'), \r\n"
                        f.write(txt3)

                if deep != 0 and deep <= 3:
                    continue

                url4 = url + str(code) + "/" + str(code2)[2:4] + "/" + str(code3) + ".html"
                print(url4)
                html4 = fetchHTML(url4, headers)
                codes4 = re.findall(re_town_1, html4)
                names4 = re.findall(re_town_2, html4)

                if (deep >= 4 and deep < 6) and onlyStat:
                    stat += len(codes4)
                    stat_town += len(codes4)
                    continue

                txt4 = ""
                for code4, name4 in zip(codes4, names4):

                    if level in [4]:
                        if int(str(code4)[0:9]) not in listHandle(data, 0, 9):
                            continue

                    if (level == 0 or pow(2, level) < pow(2, 4)) and  (deep == 0 or (deep >= 4 and deep < 6)):
                        stat += 1
                        stat_town += 1
                        if onlyStat != True:
                            if stat % statement == 0:
                                txt4 += "('" + str(code4) + "', '" + name4 + "'); \r\n" + base_sql
                            else:
                                txt4 += "('" + str(code4) + "', '" + name4 + "'), \r\n"
                            #f.write(txt4)

                    if deep >= 5:
                        url5 = url + str(code) + "/" + str(code2)[2:4] + "/" + str(code3)[4:6] + "/" + str(code4)[0:9] + ".html"
                        print(url5)
                        html5 = fetchHTML(url5, headers)
                        codes5 = re.findall(re_village_1, html5)
                        names5 = re.findall(re_village_2, html5)
                        codes52 = re.findall(re_village_3, html5)

                        stat += len(codes5)
                        stat_village += len(codes5)
                        if onlyStat != True:
                            txt5 = ""
                            for code5, name5, code52 in zip(codes5, names5, codes52):
                                if stat % statement == 0:
                                    txt5 += "('" + str(code5) + "', '" + name5 + "', '" + str(
                                        code52) + "'); \r\n" + base_sql
                                else:
                                    txt5 += "('" + str(code5) + "', '" + name5 + "', '" + str(code52) + "'), \r\n"
                            f.write(txt5)
                f.write(txt4)

        stat_txt = '--{"code":"' + str(code) + '","name":"' + name + '","stat_province":' + str(stat_province) + ',"stat_city":' + str(stat_city) + ',"stat_county":' + str(stat_county) + ',"stat_town":' + str(stat_town) + ',"stat_village":' + str(stat_village) + "} \r\n"
        print(stat_txt)
        f.write(stat_txt)

        # 抓取完一个省的数据后，让系统暂停1秒，防止目标网站因频繁请求被拦截。
        time.sleep(1)

    f.close()

    """
    # 替换SQL语句最后的 "," 为 ";"
    f2 = open(file, "r", encoding=encoded)
    d = f2.read()
    data = d[0:len(d)-3]+";" # ,\n
    f2.close()

    f3 = open(file, "w+", encoding=encoded)
    f3.write(data)
    f3.close()
    """

    print("总数据量: " + str(stat))
    print("没有区的数据量: " + str(stat_no_county))
    print("startZhuQu timeEnd: " + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
    diffTime = time.perf_counter() - timeStart
    print("耗时: " + str(diffTime) + "s")

    return stat


"""
方法二 根据抓取失败的链接，解析得到数组，然后重新抓取数据 
# 参数说明同方法一

# return 返回数据总条数
"""
def startZhuQuFailURL(url, file, headers, deep=4, data=[], onlyStat=False, encoding="gb2312"):
    count = {"province": 0, "city": 0, "county": 0, "town": 0}

    if len(data["province"]) > 0:
        count["province"] = startZhuQu(url, file, headers, deep, 1, data["province"], onlyStat, encoding)

    if len(data["city"]) > 0:
        count["city"] = startZhuQu(url, file, headers, deep, 2, data["city"], onlyStat, encoding)

    if len(data["county"]) > 0:
        count["county"] = startZhuQu(url, file, headers, deep, 3, data["county"], onlyStat, encoding)

    if len(data["town"]) > 0:
        count["town"] = startZhuQu(url, file, headers, deep, 4, data["town"], onlyStat, encoding)

    return count

"""
方法三 采用多线程抓取数据
# 参数说明同方法一

# return 返回data数据
"""
def startFetchVillageWithThread(url, file, headers, deep=0, level=0, data=[], onlyStat=False, encoding="gb2312"):
    dataList = []
    key = ""
    if len(data["province"]) > 0:
        dataList = data["province"]
        key = "province"

    if len(data["city"]) > 0:
        dataList = data["city"]
        key = "city"

    if len(data["county"]) > 0:
        dataList = data["county"]
        key = "county"


    if len(data["town"]) > 0:
        dataList = data["town"]
        key = "town"

    threads = []
    # 创建新线程
    for d in dataList:
        f  = d + ".sql"
        p = []
        p.append(d)
        # 创建新线程
        thread = fetchVillageWithThread(url, file+f, headers, deep, level, p, onlyStat, encoding)
        thread.start()
        threads.append(thread)

    # 等待所有线程完成
    for t in threads:
        t.join()

    print("退出主线程")
    return data

"""
方法四：多线程执行
"""
class fetchVillageWithThread(threading.Thread):
    def __init__(self, url, file, headers, deep, level, data, onlyStat=False, encoding="gb2312"):
        threading.Thread.__init__(self)

        self.url = url
        self.file = file
        self.headers = headers
        self.deep = deep
        self.level = level
        self.data = data
        self.encoding = encoding
        self.onlyStat = onlyStat

    def run(self):
        print ("开始线程：" + self.file)
        print(self.data)
        startZhuQu(self.url, self.file, self.headers, self.deep, self.level, self.data, self.onlyStat, self.encoding)
        print ("退出线程：" + self.file)

