-- Danny 2018-11-30

-- 查询{省}
SELECT *
FROM  `china_region_four`
WHERE  `code` LIKE  '%0000000000'

-- 根据{省}查询{市}，例如 广东省44
SELECT *
FROM  `china_region_four`
WHERE  `code` LIKE  '44%00000000'
AND `code` != '440000000000'

-- 根据{省市}查询{区}，例如 广东省广州市4401
SELECT *
FROM  `china_region_four`
WHERE  `code` LIKE  '4401%000000'
AND  `code` !=  '440100000000'

-- 根据{省市区}查询{街道}，例如 广东省广州市荔湾区440103
SELECT *
FROM  `china_region_four`
WHERE  `code` LIKE  '440103%000'
AND  `code` !=  '440103000000'

-- 根据{省市区街道}查询{居委会}，例如 广东省广州市荔湾区彩虹街道440103009
SELECT *
FROM  `china_region_village`
WHERE  `code` LIKE  '440103009%'
AND  `code` !=  '440103009000'

-- 统计{省}
SELECT COUNT( * )
FROM  `china_region_four`
WHERE  `code` LIKE  '%0000000000'

-- 统计{省}下面的{市}，例如 广东省44
SELECT COUNT( * )
FROM  `china_region_four`
WHERE  `code` LIKE  '44%00000000'
AND `code` != '440000000000'

-- 统计{省市}下面的{区}，例如 广东省广州市4401
SELECT COUNT( * )
FROM  `china_region_four`
WHERE  `code` LIKE  '4401%000000'
AND  `code` !=  '440100000000'

-- 统计{省市区}下面的{街道}，例如 广东省广州市荔湾区440103
SELECT COUNT( * )
FROM  `china_region_four`
WHERE  `code` LIKE  '440103%000'
AND  `code` !=  '440103000000'

-- 统计{省市区街道}下面的{居委会}，例如 广东省广州市荔湾区彩虹街道440103009
SELECT COUNT( * )
FROM  `china_region_village`
WHERE  `code` LIKE  '440103009%'
AND  `code` !=  '440103009000'
