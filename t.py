#!/usr/bin/env Python3
# -*- coding: gb2312 -*-

"""
# Danny 2018-11-30 Aiitec
# 从国家统计局网站抓取{省市区街道居委会}五级地理位置数据
# http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/index.html
"""

import sys, os, time, re

from func import logListHandle
from func import listHandle

"""
f = open("data/log-error.log", "r", encoding="gb2312")
log = f.read()
data = logListHandle(log)
print(data)

list = listHandle(data["city"],0,2)
print(list)

list2 = listHandle(data["county"],0,4)
print(list2)

timeStart = time.perf_counter()
time.sleep(1)
timeEnd = time.perf_counter()
diffTime = timeEnd - timeStart
print("耗时: " + str(diffTime) + "s")
"""

"""
tr = "[HTTPError 2018-11-30 16:59:00] http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/44/19/441900124.html\r\n"
tr += "[HTTPError 2018-11-30 16:10:00] http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/44/19/441900125.html"
txt = re.sub(r"\[.*\] ", "", tr)
print("电话号码 : ", txt)
"""
