# 中国行政区域数据抓取项目 WIKI 文档

> 数据源：国家统计局 http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/index.html

[toc]
```
中国行政区域数据抓取项目 WIKI 文档
    一、2017年行政区划数据统计数据图表
    二、项目使用说明
        1. 抓取{省市区街道}四级数据
        2. 重新抓取失败的URL{省市区街道}四级数据
        3. 仅抓取五级{居委会}数据
        4. 重新抓取失败的URL{居委会}五级数据（
        5. 统计{省市区街道} 或 {居委会} 数据
        6. 重新统计失败的URL{省市区街道} 或 {居委会} 数据
    三、项目文件结构说明
    四、数据完整性校验方法
```

## 一、2017年行政区划数据统计数据图表
2017年行政区划数据统计数据：[https://img-blog.csdnimg.cn/20181204175514480.png](https://img-blog.csdnimg.cn/20181204175514480.png)
![2017年行政区划数据统计](/uploads/89c2a2f6bd6375b0b1d6dc1c8d70c0fc/2017年行政区划数据统计.png)

## 二、项目使用说明

### 1. 抓取{省市区街道}四级数据
```
# 抓取{省市区街道}四级数据
count = startZhuQu(base_url, dataFile, my_headers)
# 抓取{省市区}三级数据
#count = startZhuQu(base_url, dataFile, my_headers, 3)
# 抓取{省市区街道}四级数据
#count = startZhuQu(base_url, dataFile, my_headers, 4)
# 仅抓取{居委会}第五级数据
#count = startZhuQu(base_url, dataFile, my_headers, 6)

print(count)
```
### 2. 重新抓取失败的URL{省市区街道}四级数据
```
logFile = open("data/log-error.log", "r", encoding=encoded)
log = logFile.read()
data = logListHandle(log)
count = startZhuQuFailURL(base_url, dataFile, my_headers, 4, data)

print(count)
```

### 3. 仅抓取五级{居委会}数据
```
# 1. 仅抓取五级{居委会}全部数据
count = startZhuQu(base_url, dataFile5, my_headers, 6)
print(count)

# 2. 仅抓取五级{居委会}指定数据
count = startZhuQu(base_url, dataFile5, my_headers, 6, 1, ['11'])
#count = startZhuQu(base_url, dataFile5, my_headers, 6, 2, ['4419', '4420', '4604']) # 广东省东莞市 广东省中山市 海南省儋州市
#count = startZhuQu(base_url, dataFile5, my_headers, 6, 3, ['110101', '110102'])
#count = startZhuQu(base_url, dataFile5, my_headers, 6, 4, ['441900124'])

print(count)
```

### 4. 重新抓取失败的URL{居委会}五级数据（
```
logFile = open("data/log-error.log", "r", encoding=encoded)
log = logFile.read()
data = logListHandle(log)
print(data)
count = startZhuQuFailURL(base_url, dataFile5, my_headers, 6, data)

print(count)
```

### 5. 统计{省市区街道} 或 {居委会} 数据
```
# 1. 统计{省市区街道} 数据
count = startZhuQu(base_url, statFile, my_headers, 0, 0, [], True, encoding=encoded)
# 2. 统计{居委会} 数据
#count = startZhuQu(base_url, statFile, my_headers, 6, 0, [], True, encoding=encoded)

print(count)
```

### 6. 重新统计失败的URL{省市区街道} 或 {居委会} 数据
```
logFile = open("data/log-error.log", "r", encoding=encoded)
log = logFile.read()
data = logListHandle(log)

# 1.重新统计失败的URL{省市区街道}四级数据
count = startZhuQuFailURL(base_url, statFile, my_headers, 4, data, True)

# 2. 重新统计{居委会} 数据
count = startZhuQuFailURL(base_url, statFile, my_headers, 6, data, True)

print(count)
```

## 三、项目文件结构说明

文件（夹） | 文件 | 概述
---|---|---
根目录 | index.py | 程序主入口
&ensp; | func.py | 公共函数库，提供：<br />方法一：抓取网页内容fetchHTML<br /> 方法二：解析错误日志记录，并返回数据列表logListHandle<br /> 方法三：数据列表去重listHandle
&ensp; | fetchHTML.py | 抓取并解析HTML的主文件，提供：<br />方法一：抓取{省市区街道居委会}五级地理位置数据startZhuQu：<br />方法二：根据抓取失败的链接，解析得到数组，然后重新抓取数据startZhuQuFailURL<br />方法三：采用多线程抓取数据startFetchVillageWithThread<br />方法四：多线程执行fetchVillageWithThread
&ensp; | t.py | 测试程序用，可忽略
sql/ | [china_region_three.sql](https://gitlab.aiitec.org/python/orgin-prj/blob/master/sql/china_region_three.sql) | {省市区}三级 数据sql语句（含港澳台数据）
&ensp; | [china_region_four.sql](https://gitlab.aiitec.org/python/orgin-prj/blob/master/sql/china_region_four.sql) | {省市区街道}四级 数据sql语句（含港澳台数据）
&ensp; | [china_region_village_670767.sql](https://gitlab.aiitec.org/python/orgin-prj/blob/master/sql/china_region_village_670767.sql) | 纯{居委会}第五级数据sql语句
&ensp; | [china_region_gat_6.sql](https://gitlab.aiitec.org/python/orgin-prj/blob/master/sql/china_region_gat_6.sql) | 纯港澳台{省市区}三级数据sql语句，编号索引6位
&ensp; | [china_region_gat.sql](https://gitlab.aiitec.org/python/orgin-prj/blob/master/sql/china_region_gat.sql) | 纯港澳台{省市区}三级数据sql语句，编号索引12位
&ensp; | [SQL脚本.sql](https://gitlab.aiitec.org/python/orgin-prj/blob/master/sql/SQL%E8%84%9A%E6%9C%AC.sql) | 各种查询、统计sql语句示例
data/ | 1234.sql | 抓取的{省市区街道}四级数据
&ensp; | 5.sql | 抓取的{居委会}第五级数据
&ensp; | stat.log | 统计数据
&ensp; | log-error.log | 抓取错误的URL
&ensp; | stat-error-data.log | 抓取错误的URL转格式化后的python字典
统计局数据分析/ | 0国家统计局 | 抓取{省市区街道居委会}五级数据时的一些分析资料
&ensp; | 1省 | 抓取{省}数据，需要解析的HTML格式
&ensp; | 2市 | 抓取{市}数据，需要解析的HTML格式
&ensp; | 3区 | 抓取{区}数据，需要解析的HTML格式
&ensp; | 4街道 | 抓取{街道}数据，需要解析的HTML格式
&ensp; | 5居委会 | 抓取{居委会}数据，需要解析的HTML格式
&ensp; | china_region.xlsx | 经过数据校验后的各级数据量统计

## 四、数据完整性校验方法
- 第一步：抓取数据并写入数据库
- 第二步：数据库统计，分别统计数据库中{省市区街道居委会}各级数据量
- 第三步：程序统计，全面跑一遍程序统计出{省市区街道居委会}各级数据量
- 第四步：对比 数据库统计 的数据与 程序统计 的数据是否完全一致，验证并得出数据完整性结论